#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 10:28 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : functions.py
@Software: PyCharm
"""
import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(x))


class LastSquare:
    name = "lastSquare"

    # def __init__(self, train_data, train_labels, model,
    #              learn_rate=0.0001, epsilon=0.00000001,
    #              alpha=0.9, beta=0.99, batch_size=200, max_iter=50):
    #     self.model = model
    #     self.train_data = train_data
    #     self.train_labels = train_labels
    #     self.learn_rate = learn_rate
    #     self.alpha = alpha
    #     self.beta = beta
    #     self.epsilon = epsilon
    #     self.batch_size = batch_size
    #     self.max_iter = max_iter
    #     self.w = np.zeros((self.train_data.shape[1]))

    def loss(train_data, train_labels, w):
        res = np.dot(train_data, w) - train_labels
        return 0.5 * np.dot(res.T, res)

    def gradient(train_data, train_labels, w):
        return np.dot(train_data.T, (np.dot(train_data, w) - train_labels))


class LogisticRegression:
    name = "lr"

    def loss(train_data, train_labels, w):
        res = np.dot(train_data, w)
        loss = (-train_labels * res).sum() + np.log(1 + np.exp(res[res < 10])).sum() + res[res > 10].sum()
        return loss

    def gradient(train_data, train_labels, w):
        return np.dot(train_data.T,
                      1 - sigmoid(np.dot(train_data, w)) - train_labels)


class Softmax:
    name = "softmax"

    def loss(in_label, train_labels):
        res = in_label
        loss = (-train_labels * res).sum() + np.log(1 + np.exp(res[res < 10])).sum() + res[res > 10].sum()
        return loss

    def gradient(in_label, train_labels):
        # print("func",in_label.shape,train_labels.shape)
        # in_label = in_label.reshape(in_label.shape[0])
        return 1 - sigmoid(in_label) - train_labels

    def gradient_square(in_label, train_labels):
        return in_label - train_labels
