#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 06/08/2017 10:09 PM
@Author  : Devzh
@Site    : 
@File    : optimizers.py
@Software: PyCharm
"""

import numpy as np


class Optimizers(object):
    def __init__(self, batch_size=0, max_iter=50, name="optimizer", **kwargs):
        self.batch_size = batch_size
        self.max_iter = max_iter
        self.graph = None
        self.iterations = 0
        self.N = None
        self.epoch_size = None
        self.total_rounds = None
        self.logger = []
        self.name = name

    def get_name(self):
        return self.name

    def _predict(self, data, label):
        self.graph.set_running_data(data, label)
        self.graph.forward()
        return self.graph.get_loss()

    def _run_once(self):
        self.graph.forward()
        self.graph.back()
        # self.graph.get_weight()
        self.iterations += 1
        self.graph.update_weight(self.get_updates())

    def _run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")
        self.run()

    def _log(self):
        # 每个epoch_size内计算三个点，用于logging
        if self.iterations * 3 % self.epoch_size == 0:
            loss = self._predict(self.graph.train_data, self.graph.train_label)
            self.logger.append((self.iterations / self.epoch_size, loss))
            print("iterations\t", self.iterations, "\tloss\t", loss)

    def get_logger(self):
        array = np.zeros((len(self.logger), 2))
        for i in range(0, len(self.logger)):
            array[i][0] = self.logger[i][0]
            array[i][1] = self.logger[i][1]
        return array

    def init(self, graph=None, **kwargs):
        if self.graph is None:
            if graph is not None:
                self.graph = graph
                self.N = self.graph.train_data.shape[0]
                if self.batch_size == 0:
                    self.batch_size = self.N
                self.epoch_size = int(self.N / self.batch_size)
                self.total_rounds = int(self.max_iter * self.epoch_size)
            else:
                raise ValueError("optimizer's model is None, please build first!")

    def grad_init(self):
        raise NotImplementedError

    def get_updates(self, **kwargs):
        raise NotImplementedError

    def run(self, **kwargs):
        raise NotImplementedError
