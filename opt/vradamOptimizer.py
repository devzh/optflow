#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    :  2/6/18 9:55 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : vradamOptimizer.py
@Software: PyCharm
"""
import random
import numpy as np
from optflow.opt.optimizers import Optimizers


class VrAdam(Optimizers):
    """
    After SAGA gradient, No momentum on gradient.
    """

    def __init__(self, lr=0.0001, batch_size=0, max_iter=100, epsilon=1e-8,
                 nesterov=False, **kwargs):
        super(VrAdam, self).__init__(batch_size=batch_size,
                                     max_iter=max_iter, name="VrAdam", **kwargs)
        self.nesterov = nesterov
        self.lr = lr
        self.epsilon = epsilon
        self.index = None
        self.amount = 0
        self.amount_square = 0
        self.grad_list = []
        self.square_list = []
        self.now_rands = 0

    def grad_init(self):
        for rand in range(0, self.epoch_size):
            """range not include last number"""
            data = self.graph.train_data[
                   int(rand * self.batch_size):
                   int((rand + 1) * self.batch_size), :]
            label = self.graph.train_label[
                    int(rand * self.batch_size):
                    int((rand + 1) * self.batch_size)]

            self.graph.set_running_data(data, label)
            self.graph.forward()
            self.graph.back()
            grad = self.graph.get_gradient()
            square = self.graph.get_grad_square()
            self.grad_list.append(grad)
            self.square_list.append(square)
            self.amount = self.amount + grad
            self.amount_square = self.amount_square + square

    def run(self):
        self.grad_init()

        last_rand = None
        print("rounds:\t", self.total_rounds)
        for i in range(0, self.total_rounds):
            self.now_rands = i + 1
            rand = random.randint(0, self.epoch_size - 1)
            self.index = rand
            if rand != last_rand:
                data = self.graph.train_data[
                       int(rand * self.batch_size):
                       int((rand + 1) * self.batch_size), :]
                label = self.graph.train_label[
                        int(rand * self.batch_size):
                        int((rand + 1) * self.batch_size)]
                self.graph.set_running_data(data, label)

            self._run_once()
            # if i == 3:
            #     break
            last_rand = rand
            self._log()

    def get_updates(self):
        grads = self.graph.get_gradient()
        square = self.graph.get_grad_square()

        diff_grads = grads - self.grad_list[self.index]

        m = diff_grads + self.amount / self.epoch_size
        v = square / self.now_rands + (1 - 1 / self.now_rands) * self.amount_square

        lr = -self.lr
        delta_grad = 0.0 * m
        for i in range(0, len(m)):
            delta_grad[i] = lr * m[i] / (np.sqrt(v[i]) + self.epsilon)

        self.amount = self.amount + diff_grads
        self.amount_square = v
        self.grad_list[self.index] = grads + 0
        # self.square_list[self.index] = square + 0
        """ +0 for gain a data reference 
        """

        # return lr * m
        return delta_grad
