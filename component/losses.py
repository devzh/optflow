#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 10:26 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : losses.py
@Software: PyCharm
"""

import numpy as np
import optflow.core.operators as ops
from optflow.core.compute_graph import Node


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class Loss_square:
    def __init__(self, node_pre, node_labels):
        self.diff = Node(operator=ops.Sub(), node_a=node_pre, node_b=node_labels)
        self.loss = Node(operator=ops.Power(2), node_a=self.diff, node_type="end")

    def run(self):
        return self.loss


class Loss_softmax:
    def __init__(self, node_pre, node_labels):
        pass


class Cross_entropy:
    pass
