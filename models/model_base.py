#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 9/1/17 3:54 PM
@Author  : zhui
@Site    : devzh.cn
@File    : mode_base.py
"""
from optflow.core.compute_graph import Node
from optflow.core.compute_graph import ComputeGraph


class Base(object):
    def __init__(self, name, train_data, train_labels,
                 optimizer, **kwargs):
        self._name = name
        self.optimizer = optimizer
        self.running_data = Node(node_type="constant")
        self.running_label = Node(node_type="constant")
        self.endNode = None

        self.graph = ComputeGraph()
        """build a DAG compute graph from constant node to endNode"""
        self.build()

        self.graph.set_data_node(self.running_data)
        self.graph.set_label_node(self.running_label)
        self.graph.set_end_node(self.endNode)
        self.graph.set_model_data(train_data, train_labels)
        self.graph.topo_sort()

    def build(self):
        """
        suppose to call: self._build(end_node)
        :return: build compute graph 
        """
        raise NotImplementedError

    def _build(self, end_node):
        end_node.gradient = 1
        self.endNode = end_node

    def train(self):
        if self.endNode is not None:
            # self.one_round(epoch_size, total_rounds)
            self.optimizer.init(self.graph)
            self.optimizer.run()
        else:
            raise ValueError("Compute Graph end is None, please build first")

    def get_model_name(self):
        return self._name

    def add_variable(self, node):
        self.graph.add_variable(node)
