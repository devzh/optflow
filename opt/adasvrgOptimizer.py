#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 08/02/2018 6:06 AM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : adasvrgOptimizer.py
@Software: PyCharm
"""
import random
import numpy as np
from optflow.opt.optimizers import Optimizers


class AdaSVRG(Optimizers):
    """Stochastic gradient descent optimizer.

    Includes support for momentum,
    learning rate decay, and Nesterov momentum.

    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0.001, batch_size=0, max_iter=100,
                 epsilon=1e-8, **kwargs):
        super(AdaSVRG, self).__init__(batch_size=batch_size,
                                      max_iter=max_iter, name="AdaSVRG", **kwargs)
        self.lr = lr
        self.epsilon = epsilon
        self.index = None
        self.mu = 0
        self.square = 0
        self.grad_list = []

    def run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")

        last_rand = None
        print("epochs:\t", self.max_iter)
        for epo in range(0, int(self.max_iter / 6)):
            self.grad_init()
            for i in range(0, self.epoch_size * 5):
                rand = random.randint(0, self.epoch_size - 1)
                # rand = 1
                self.index = rand
                if rand != last_rand:
                    data = self.graph.train_data[
                           int(rand * self.batch_size):
                           int((rand + 1) * self.batch_size), :]
                    label = self.graph.train_label[
                            int(rand * self.batch_size):
                            int((rand + 1) * self.batch_size)]

                    self.graph.set_running_data(data, label)

                self._run_once()
                last_rand = rand
                self._log()

    def get_updates(self):
        grads = self.graph.get_gradient()
        diff_grad = grads - self.grad_list[self.index] + self.mu

        delta_grad = diff_grad * 0
        for i in range(0, len(diff_grad)):
            delta_grad[i] = diff_grad[i] / (np.sqrt(self.square[i]) + self.epsilon)

        delta_grad = - self.lr * delta_grad

        return delta_grad

    def grad_init(self):
        for rand in range(0, self.epoch_size):
            """range not include last number"""
            data = self.graph.train_data[
                   int(rand * self.batch_size):
                   int((rand + 1) * self.batch_size), :]
            label = self.graph.train_label[
                    int(rand * self.batch_size):
                    int((rand + 1) * self.batch_size)]

            self.graph.set_running_data(data, label)
            self.graph.forward()
            self.graph.back()
            grad = self.graph.get_gradient()
            self.grad_list.append(grad)
            self.mu = self.mu + grad / self.epoch_size
            self.square = self.square + (grad * grad) / self.epoch_size
