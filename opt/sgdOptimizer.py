#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 9:56 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : sgdOptimizer.py
@Software: PyCharm
"""
import random
from optflow.opt.optimizers import Optimizers


class SGD(Optimizers):
    """Stochastic gradient descent optimizer.

    Includes support for momentum,
    learning rate decay, and Nesterov momentum.

    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0.001, momentum=0.95, decay=0.01, batch_size=0, max_iter=100,
                 nesterov=False, **kwargs):
        super(SGD, self).__init__(batch_size=batch_size, max_iter=max_iter, name="SGD", **kwargs)
        self.nesterov = nesterov
        self.momentum = momentum
        self.lr = lr
        self.decay = decay
        self.momentum_grad = None

    def run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")

        last_rand = None
        print("rounds:\t", self.total_rounds)
        for i in range(0, self.total_rounds):
            rand = random.randint(0, self.epoch_size - 1)
            # rand = 1
            if rand != last_rand:
                data = self.graph.train_data[
                       int(rand * self.batch_size):
                       int((rand + 1) * self.batch_size), :]
                label = self.graph.train_label[
                        int(rand * self.batch_size):
                        int((rand + 1) * self.batch_size)]

                self.graph.set_running_data(data, label)

            self._run_once()
            # if i == 0:
            #     break
            last_rand = rand
            self._log()

    def get_updates(self):
        lr = self.lr
        lr *= (1. / (1. + self.decay * self.iterations))

        grads = self.graph.get_gradient()
        # print("SUM", grads[1])

        if self.momentum_grad is None:
            self.momentum_grad = 0.0 * grads

        v = self.momentum * self.momentum_grad - lr * grads  # velocity
        self.momentum_grad = v
        if self.nesterov:
            delta_grad = self.momentum * v - lr * grads
        else:
            delta_grad = v

        # delta_grad = -lr * grads
        # print(delta_grad)
        return delta_grad
