#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/7/18 2:24 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : __init__.py
@Software: PyCharm
"""

from optflow.opt.adamOptimizer import Adam
from optflow.opt.adam2Optimizer import Adam2
from optflow.opt.adasagaOptimizer import AdaSAGA
from optflow.opt.ppproAdasagaOptimizer import PPProAdaSAGA
from optflow.opt.pproAdasagaOptimizer import PProAdaSAGA
from optflow.opt.proAdasagaOptimizer import ProAdaSAGA
from optflow.opt.sagaOptimizer import SAGA
from optflow.opt.sgdOptimizer import SGD
from optflow.opt.vradamOptimizer import VrAdam
from optflow.opt.svrgOptimizer import SVRG
from optflow.opt.adasvrgOptimizer import AdaSVRG
