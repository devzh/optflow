#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 8/9/17 8:59 AM
@Author  : zhui
@Site    : devzh.cn
@File    : mlp.py
"""
import optflow.opt as optimizers
import optflow.core.operators as ops
import optflow.component.widgets as wig
from optflow.models.model_base import Base


class MLP(Base):
    def __init__(self, train_data, train_labels,
                 hidden_net=[40, 10], act_func=ops.Sigmoid(),
                 optimizer=optimizers.SGD(lr=0.001, batch_size=300, max_iter=50)):
        self.N = train_data.shape[0]
        self.D = train_data.shape[1]
        hidden_net.append(train_labels.shape[1])
        # hidden_net.insert(0, self.D)
        self.hidden_net = hidden_net
        self.act_func = act_func

        super(MLP, self).__init__(name="mlp", optimizer=optimizer,
                                  train_data=train_data, train_labels=train_labels)

    def build(self):
        start = self.D
        tmp_node = self.running_data
        for net_num in self.hidden_net:
            tmp_node = wig.LinearUnit(tmp_node, start, net_num,
                                      self.act_func).run(self.graph)
            print(start, "*", net_num)
            start = net_num

        sig = tmp_node
        end = wig.LossSquare(sig, self.running_label).run()
        self._build(end)
