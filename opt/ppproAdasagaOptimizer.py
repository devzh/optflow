#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 9:58 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : ppproAdasagaOptimizer.py
@Software: PyCharm
"""
import random
import numpy as np
from optflow.opt.optimizers import Optimizers


class PPProAdaSAGA(Optimizers):
    """
    Adding momentum as SAGA gradient
    """

    def __init__(self, lr=0.0001, alpha=0.9, beta=0.999, batch_size=0, max_iter=100, epsilon=1e-8,
                 nesterov=False, **kwargs):
        super(PPProAdaSAGA, self).__init__(batch_size=batch_size,
                                           max_iter=max_iter, name="PPProAdaSAGA", **kwargs)
        self.nesterov = nesterov
        self.alpha = alpha
        self.beta = beta
        self.lr = lr
        self.epsilon = epsilon
        self.momentum_grad = None
        self.momentum_square = None

        self.index = None
        self.amount = 0
        self.grad_list = []

    def grad_init(self):
        for rand in range(0, self.epoch_size):
            """range not include last number"""
            data = self.graph.train_data[
                   int(rand * self.batch_size):
                   int((rand + 1) * self.batch_size), :]
            label = self.graph.train_label[
                    int(rand * self.batch_size):
                    int((rand + 1) * self.batch_size)]

            self.graph.set_running_data(data, label)
            self.graph.forward()
            self.graph.back()
            grad = self.graph.get_gradient()
            self.grad_list.append(grad)
            self.amount = self.amount + grad

    def run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")
        self.grad_init()

        last_rand = None
        print("rounds:\t", self.total_rounds)
        for i in range(0, self.total_rounds):
            rand = random.randint(0, self.epoch_size - 1)
            self.index = rand
            if rand != last_rand:
                data = self.graph.train_data[
                       int(rand * self.batch_size):
                       int((rand + 1) * self.batch_size), :]
                label = self.graph.train_label[
                        int(rand * self.batch_size):
                        int((rand + 1) * self.batch_size)]
                self.graph.set_running_data(data, label)

            self._run_once()
            # if i == 2:
            #     break
            last_rand = rand
            self._log()

    def get_updates(self):
        grads = self.graph.get_gradient()
        square = self.graph.get_grad_square()
        if self.momentum_grad is None:
            self.momentum_grad = 0.0 * grads
            self.momentum_square = 0.0 * square

        # m = grads - self.grad_list[self.index] + self.amount / self.epoch_size
        m = grads
        v = self.beta * self.momentum_square + (1 - self.beta) * square

        self.momentum_grad = self.alpha * grads + (1 - self.alpha) * m
        self.momentum_square = v

        lr = - np.sqrt(
            1 - np.power(self.beta, self.iterations)) / (1 - np.power(self.alpha, self.iterations))

        delta_grad = 0.0 * m
        for i in range(0, len(m)):
            delta_grad[i] = lr * self.momentum_grad[i] / (np.sqrt(v[i]) + self.epsilon)

        tmp_delta = delta_grad - self.grad_list[self.index]
        self.amount = self.amount + tmp_delta
        self.grad_list[self.index] = delta_grad + 0
        """ +0 for gain a data reference 
        """

        return self.lr * (tmp_delta + self.amount / self.epoch_size)
