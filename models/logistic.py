#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 8/10/17 4:36 PM
@Author  : zhui
@Site    : devzh.cn
@File    : logistic.py
"""
import optflow.opt as optimizers
import optflow.component.widgets as wig
from optflow.models.model_base import Base


class Logistic(Base):
    def __init__(self, train_data, train_labels,
                 optimizer=optimizers.SGD(lr=0.001, batch_size=300, max_iter=50)):
        name = "logistic"
        self.D = train_data.shape[1]
        super(Logistic, self).__init__(name, train_data, train_labels, optimizer)

    def build(self):
        sig = wig.LinearUnit(node_start=self.running_data,
                             net_in=self.D, net_out=1, act_func=None).run(self.graph)
        end = wig.LossSquare(sig, self.running_label).run()
        # end = LossLogistic(sig, self.running_label).run()
        self._build(end)
