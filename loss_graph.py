#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 9/1/17 4:23 PM
@Author  : zhui
@Site    : www.devzh.cn
@File    : loss_graph.py
"""

import numpy as np
import matplotlib.pyplot as plt
import optflow.opt as optimizers
from optflow.models.mlp import MLP
from dataset import mnist_input as mnist


def compare_loss(*args):
    print(len(args))
    loss_list = []
    color_list = ["r_--", "b+:", "g*:", "y", "k", "c"]
    for name in args:
        loss = np.loadtxt("../results/" + name + "-mlp-loss.txt")
        loss_list.append(loss)

    plt.title("loss graph")
    plt.xlabel("epochs")
    plt.ylabel("loss")

    for i in range(len(args)):
        plt.plot(loss_list[i][:, 0], loss_list[i][:, 1], color_list[i], label=args[i])

    plt.legend()
    plt.show()


def run_unit(model, optimizer, run_status=False, show_status=True):
    if run_status:
        train_data, train_label = mnist.train_init(one_hot=True)
        print(train_data.shape, train_label.shape)
        run = model(train_data, train_label, optimizer=optimizer, hidden_net=[100])
        run.train()
        loss_graph = optimizer.get_logger()
        np.savetxt("../results/" + optimizer.name + "-"
                   + run.get_model_name() + "-loss.txt", loss_graph)

    if show_status:
        loss_graph = np.loadtxt("../results/" + optimizer.name + "-"
                                + run.get_model_name() + "-loss.txt")
        plt.title('loss graph')
        plt.xlabel('epochs')
        plt.ylabel('loss')

        plt.plot(loss_graph[:, 0], loss_graph[:, 1], 'b', label=optimizer.name)
        plt.show()


if __name__ == "__main__":
    # test_data, test_label = mnist.train_init(one_hot=True)
    test_data, test_label = mnist.train_init()
    # test_data, test_label = inputdata.simple_iris()

    # opt = optimizers.SGD(lr=0.0001, batch_size=200, max_iter=50)
    # opt2 = optimizers.SAGA(lr=0.0001, batch_size=200, max_iter=50)

    opt = optimizers.VrAdam(lr=0.001, batch_size=200, max_iter=40)
    opt3 = optimizers.Adam(lr=0.001, batch_size=200, max_iter=40, beta=0.99)
    opt4 = optimizers.AdaSAGA(lr=0.001, batch_size=200, max_iter=40, beta=0.99)
    opt5 = optimizers.SVRG(lr=0.0001, batch_size=200, max_iter=30)
    # opt6 = optimizers.PProAdaSAGA(lr=0.0001, batch_size=20, max_iter=50)
    # opt7 = optimizers.PPProAdaSAGA(lr=0.0001, batch_size=200, max_iter=50)

    # run = MLP(test_data, test_label, optimizer=opt6)
    # run.train()

    run_unit(MLP, opt5, run_status=True, show_status=False)
    # run_unit(MLP, opt4, run_status=True, show_status=False)
    compare_loss(opt.get_name(), opt3.get_name(), opt5.get_name())
