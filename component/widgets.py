#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 8/6/17 4:33 PM
@Author  : zhui
@Site    : 
@File    : widgets.py
@Software: PyCharm

This file mainly construct high order components in machine learning models.
for example, linear_unit and logistic_unit.

@Todo CNN units
"""

import numpy as np
import optflow.core.operators as ops
from optflow.core.compute_graph import Node


def zeros_init(shape):
    """
    Initialize variables, as value are set to zero
    :param shape: the shape of variable unit
    :return: initialized variable
    """
    return np.zeros(shape)


def random_init(shape):
    """
    Initialize variables, as value are set uniformly in (-1,1)
    :param shape: the shape of variable unit
    :return: initialized variable
    """
    # print(shape)
    return np.random.uniform(-1.0, 1.0, shape)


class LossSquare:
    def __init__(self, node_pre, node_labels):
        self.diff = Node(operator=ops.Sub(), node_a=node_pre, node_b=node_labels)
        self.loss = Node(operator=ops.Power(2), node_a=self.diff, node_type="end")

    def run(self):
        return self.loss


class LossLogistic:
    def __init__(self, node_pre, node_labels):
        self.la = Node(operator=ops.Mul(), node_a=node_pre, node_b=node_labels)
        self.lb = Node(operator=ops.LnExp(), node_a=node_pre)
        self.loss = Node(operator=ops.Sub(), node_a=self.lb, node_b=self.la, node_type="end")

    def run(self):
        return self.loss


class LinearUnit:
    """
    different from norm operator. it contains node point
    """

    def __init__(self, node_start, net_in, net_out, act_func=None, init_method=random_init):

        """
        :param node_start: start node 
        :param net_num: self.D -> net_num
        :param act_func: activation function
        """
        self.net_in = net_in
        self.net_out = net_out
        self.act_func = act_func

        self.multi_0 = Node(data=init_method([self.net_in, self.net_out]),
                            # np.random.uniform(-1.0, 1.0, (self.net_in, self.net_out)),
                            node_type="variable")
        self.multi_1 = Node(operator=ops.Dot(), node_a=node_start, node_b=self.multi_0)
        self.add_0 = Node(data=init_method(self.net_out),
                          node_type="variable")
        self.add_1 = Node(operator=ops.Add(), node_a=self.multi_1, node_b=self.add_0)

        if self.act_func is not None:
            self.act_0 = Node(operator=self.act_func, node_a=self.add_1)

    def run(self, graph=None):
        if graph is not None:
            graph.add_variable(self.multi_0)
            graph.add_variable(self.add_0)
        if self.act_func is not None:
            return self.act_0
        else:
            return self.add_1


if __name__ == "__main__":
    k = random_init([2, 3])
    print(k)
