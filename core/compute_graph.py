#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 8/2/17 11:15 AM
@Author  : zhui
@Site    : 
@File    : compute_graph.py
@Software: PyCharm
"""
import numpy as np


class ComputeGraph:
    def __init__(self):
        self.var_list = []
        self.topo_order = []
        self.data_node = None
        self.label_node = None
        self.end_node = None
        self.train_data = None
        self.train_label = None
        self.gradients = np.empty(0, dtype=object)

    def set_data_node(self, node):
        self.data_node = node

    def set_label_node(self, node):
        self.label_node = node

    def set_end_node(self, node):
        self.end_node = node

    def set_model_data(self, train, label):
        self.train_data = train
        self.train_label = label

    def set_running_data(self, train, label):
        self.data_node.set_data(train)
        self.label_node.set_data(label)

    def add_variable(self, node):
        self.var_list.append(node)

    def get_weight(self):
        weight = []
        for node in self.var_list:
            # print("weight_shape \t\t",node.data.shape)
            weight.append(node.data)
        return weight

    def get_loss(self):
        return self.end_node.data.sum()

    def get_gradient(self):
        if len(self.gradients) != 0:
            for i in range(0, len(self.var_list)):
                node = self.var_list[i]
                self.gradients[i] = node.gradient
                # print("gard_shape \t\t", node.gradient.shape)
        else:
            for i in range(0, len(self.var_list)):
                grad = self.var_list[i].gradient
                # print("init gard_shape \t\t", grad.shape)
                tmp = np.empty(1, dtype=object)
                tmp[0] = grad
                self.gradients = np.append(self.gradients, tmp)

        # print(self.gradients)
        # plus 0 for no data reference
        return self.gradients + 0

    def get_grad_square(self):
        return self.gradients * self.gradients

    def update_weight(self, delta_weight):
        """
        :param delta_weight: weight add on variable 
        :return: no return
        """
        for i in range(0, len(self.var_list)):
            self.var_list[i].data = self.var_list[i].data + delta_weight[i]

    def topo_sort(self):
        """
        Topo sort the constructed compute graph nodes. returns node list
        with topo order, then we can forward or backward the compute graph
        in topo order
        :return: node list with topo order
        """
        self.visit(self.end_node)
        # print(self.topo_order)
        return self.topo_order

    def visit(self, node):
        """
        Starting at the compute graph end node, traversal the graph in post
        order
        :param node: should be the end node of compute graph
        :return: None, but append post order traversal order in self.topo_order list
        """
        if node is None:
            return
        if not node.visited:
            node.visited = True
            if node.children_a is not None:
                self.visit(node.children_a)
            if node.children_b is not None:
                self.visit(node.children_b)
            self.topo_order.append(node)
        else:
            return

    def forward(self):
        # self.end_node.forward()
        for node in self.topo_order:
            node.compute()

    def back(self):
        # self.end_node.back()
        for i in range(len(self.topo_order) - 1, 0, -1):
            node = self.topo_order[i]
            if node.operator is not None:
                "For compute node_a & node_b's gradient"
                node.operator.back(node.gradient, node.children_a, node.children_b)


class Node:
    """
    Compute Graph units: node
    """

    def __init__(self, data=None, operator=None, node_a=None, node_b=None, node_type="middle"):
        self.data = data
        self.operator = operator
        self.node_type = node_type
        self.children_a = node_a
        self.children_b = node_b
        self.gradient = None
        self.visited = False

    def set_data(self, data):
        self.data = data

    def get_child1(self):
        return self.children_a

    def get_child2(self):
        return self.children_b

    def update_weight(self):
        pass

    def add_gradient(self, grad):
        # print("Grad", grad.shape, grad)
        if self.gradient is None:
            self.gradient = grad
        else:
            self.gradient = self.gradient + grad

    def back(self):
        """
        :return: pre-order traverse, from the end node 
        """
        if self.node_type != "constant":
            # print(self.gradient)
            if self.operator is not None:
                self.operator.back(
                    self.gradient, self.children_a, self.children_b)

            if self.children_a is not None:
                self.children_a.back()

            if self.children_b is not None:
                self.children_b.back()

            # print(self.node_type, self.gradient)
            if self.node_type == "middle":
                self.data = None
                self.gradient = None

            if self.node_type == "end":
                self.data = None

            """SGD variable update
            learn_rate = 0.0001
            """
            # if self.node_type == "variable":
            #     self.data = self.data - 0.0001 * self.gradient

    def forward(self):
        """
        :return: post-order traverse through compute graph 
        """
        if self.children_a is not None:
            self.children_a.forward()
        if self.children_b is not None:
            self.children_b.forward()

        self.compute()

    def compute(self):
        if self.node_type != "end":
            self.gradient = None

        if self.node_type == "middle" or self.node_type == "end":
            self.data = self.operator.run(self.children_a, self.children_b)
            # self.gradient = None

        # single data loss
        # if self.node_type == "end":
        #     print("loss", self.data.sum())
        return

    def __repr__(self):
        return self.data.__str__()
