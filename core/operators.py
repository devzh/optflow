#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 8/6/17 4:30 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : operators.py
@Software: PyCharm
"""

import numpy as np


class Operators(object):
    """base operators in machine learning"""

    def __init__(self, name):
        if name is not None:
            if not isinstance(name, str):
                raise ValueError('Widget name must be specified with string.')
            if len(name.strip()) != len(name) or name == '':
                raise ValueError('Widget name cannot be empty or contain space characters.')
        self._name = name

    def name(self):
        return self._name

    def run(self, *args, **kwargs):
        raise NotImplementedError()

    def back(self, *args, **kwargs):
        raise NotImplementedError()


class Dot(Operators):
    def __init__(self):
        name = "Dot"
        super(Dot, self).__init__(name)

    def run(self, node1, node2):
        """
        :param node1: input node a
        :param node2: input node b
        :return: multiply result
        """
        return np.dot(node1.data, node2.data)

    def back(self, grad, node1, node2):
        """
        :param grad: gradient of current node
        :param node1: input node a
        :param node2: input node b
        :return: compute node grad
        """
        node1.add_gradient(np.dot(grad, node2.data.T))
        node2.add_gradient(np.dot(node1.data.T, grad))


class Mul(Operators):
    def __init__(self):
        name = "mul"
        super(Mul, self).__init__(name)

    def run(self, node1, node2):
        """
        :param node1: input node a
        :param node2: input node b
        :return: multiply result
        """
        return node1.data * node2.data

    def back(self, grad, node1, node2):
        """
        :param grad: gradient of current node
        :param node1: input node a
        :param node2: input node b
        :return: compute node grad
        """
        node1.add_gradient(node2.data * grad)
        node2.add_gradient(node1.data * grad)


class Add(Operators):
    def __init__(self):
        super(Add, self).__init__("add")

    def run(self, node1, node2):
        """
        :param node1: input node a
        :param node2: input node b
        :return: ada result
        """
        return np.add(node1.data, node2.data)

    def back(self, grad, node1, node2):
        """
        :param grad: gradient of current node
        :param node1: input node a
        :param node2: input node b
        :return: grad
        """
        if node1.data.shape == grad.shape:
            node1.add_gradient(grad)
            # node1.gradient = grad
        else:
            node1.add_gradient(np.sum(grad, grad.ndim - 2))
            # node1.gradient = np.sum(grad, 0)

        if node2.data.shape == grad.shape:
            node2.add_gradient(grad)
            # node2.gradient = grad
        else:
            # print("Add shape debug", node2.data.shape, grad.shape)
            node2.add_gradient(np.sum(grad, grad.ndim - 2))
            # node2.gradient = np.sum(grad, grad.ndim - 2)
            """
            @warming : add operator may contains expansion, need sum.
                but which dimension to sum, need be carefully. 
            """


class Sub(Operators):
    def __init__(self):
        super(Sub, self).__init__("sub")

    def run(self, A, B):
        """
        :param A: input node a
        :param B: input node b
        :return: ada result
        """
        return np.add(A.data, -B.data)

    def back(self, grad, A, B):
        """
        :param grad: gradient of current node
        :param A: input node a
        :param B: input node b
        :return: grad
        """
        A.add_gradient(grad)
        B.add_gradient(-grad)
        # A.gradient = grad
        # B.gradient = - grad


class Relu(Operators):
    """relu activation function
    """

    def __init__(self):
        super(Relu, self).__init__("relu")

    def run(self, A, B=None):
        """
        only one operand required
        :param A: the operand
        :param B:
        :return:
        """
        if B is not None:
            raise ValueError("relu only need one Operand")
        return np.maximum(A.data, 0)

    def back(self, grad, A, B=None):
        if B is not None:
            raise ValueError("relu only need one Operand")
        # A.gradient = [A.data > 0] * grad
        A.add_gradient([A.data > 0] * grad)


class Lrelu(Operators):
    """ leak relu
    :return max(data,leak_rate * data)
    """

    def __init__(self):
        self.mu = 1e-2
        super(Lrelu, self).__init__("leak_relu")

    def run(self, node_a, node_b=None):
        if node_b is not None:
            raise ValueError("lrelu only need one Operand")
        return np.maximum(node_a.data, self.mu * node_a.data)

    def back(self, grad, node_a, node_b=None):
        if node_b is not None:
            raise ValueError("lrelu only need one Operand")

        np.maximum(grad, grad * self.mu)
        # node_a.gradient = [node_a.data > 0] * grad + [node_a.data < 0] * grad * self.mu
        node_a.add_gradient([node_a.data > 0] * grad + [node_a.data < 0] * grad * self.mu)


class Log(Operators):
    """
    :return np.log(x)
    """

    def __init__(self):
        super(Log, self).__init__("log")

    def run(self, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        return np.log(A.data)

    def back(self, grad, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        # A.gradient = 1 / A.data * grad
        A.add_gradient(1 / A.data * grad)


class Exp(Operators):
    """
    :return np.exp(x)
    """

    def __init__(self):
        super(Exp, self).__init__("exp")

    def run(self, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        return np.exp(A.data)

    def back(self, grad, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        A.gradient = A.data * grad
        A.add_gradient(A.data * grad)


class LnExp(Operators):
    """
    :return ln(1+exp(x)) 
    derivative exp(x)/(1+exp(x))
    """

    def __init__(self):
        super(LnExp, self).__init__("ln(1+exp(x))")

    def run(self, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        return np.log(1 + np.exp(A.data))

    def back(self, grad, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        # A.gradient = sigmoid_function(A.data) * grad
        A.add_gradient(sigmoid_function(A.data) * grad)


def sigmoid_function(x):
    return 1.0 / (1.0 + np.exp(-x))


class Sigmoid(Operators):
    """
    :return 1/(1+exp(-x))
    """

    def __init__(self):
        super(Sigmoid, self).__init__("sigmoid")

    def run(self, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        return sigmoid_function(A.data)

    def back(self, grad, A, B=None):
        if B is not None:
            raise ValueError("sigmoid only need one Operand")
        sig = sigmoid_function(A.data)
        A.add_gradient((1 - sig) * sig * grad)
        # A.gradient = (1 - sig) * sig * grad


class Power(Operators):
    """
    :return power function, only test in square station
    """

    def __init__(self, num):
        self.p = num
        super(Power, self).__init__("power")

    def run(self, node1, node2=None):
        if node2 is not None:
            raise ValueError("power only need one Operand")
        return np.power(node1.data, self.p)

    def back(self, grad, node1, node2=None):
        if node2 is not None:
            raise ValueError("power only need one Operand")
        if self.p != -1:
            # node1.gradient = self.p * np.power(node1.data, self.p - 1) * grad
            node1.add_gradient(self.p * np.power(node1.data, self.p - 1) * grad)
        else:
            # node1.gradient = np.log(node1.data) * grad
            node1.add_gradient(np.log(node1.data) * grad)
