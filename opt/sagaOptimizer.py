#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 9:54 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : sagaOptimizer.py
@Software: PyCharm
"""

import random
from optflow.opt.optimizers import Optimizers


class SAGA(Optimizers):
    def __init__(self, lr=0.001, batch_size=0, max_iter=100):
        super(SAGA, self).__init__(batch_size=batch_size, max_iter=max_iter, name="SAGA")
        self.lr = lr
        self.index = None
        self.amount = 0
        self.grad_list = []

    def run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")

        self.grad_init()

        last_rand = None
        for i in range(0, self.total_rounds):
            rand = random.randint(0, self.epoch_size - 1)
            self.index = rand
            """randint include both number"""
            if rand != last_rand:
                data = self.graph.train_data[
                       int(rand * self.batch_size):
                       int((rand + 1) * self.batch_size), :]
                label = self.graph.train_label[
                        int(rand * self.batch_size):
                        int((rand + 1) * self.batch_size)]

                self.graph.set_running_data(data, label)

            self._run_once()
            last_rand = rand
            self._log()

    def get_updates(self):
        lr = self.lr
        # print("grad_list_check", self.grad_list[self.index][1])
        grads = self.graph.get_gradient()

        # print("list", self.grad_list[self.index][1], "\n amount", self.amount[1])
        delta_grad = grads - self.grad_list[self.index] + (1.0 / self.epoch_size) * self.amount
        self.amount = self.amount + grads - self.grad_list[self.index]
        self.grad_list[self.index] = grads + 0
        """ +0 for gain a data reference 
        """

        # print("SUM", grads[1], self.amount[1], self.graph.get_weight()[1])

        # print("grad", grads[1])

        delta_grad = - lr * delta_grad
        # delta_grad = -lr * grads

        return delta_grad

    def grad_init(self):
        for rand in range(0, self.epoch_size):
            """range not include last number"""
            data = self.graph.train_data[
                   int(rand * self.batch_size):
                   int((rand + 1) * self.batch_size), :]
            label = self.graph.train_label[
                    int(rand * self.batch_size):
                    int((rand + 1) * self.batch_size)]

            self.graph.set_running_data(data, label)
            self.graph.forward()
            self.graph.back()
            grad = self.graph.get_gradient()
            self.grad_list.append(grad)
            self.amount = self.amount + grad
