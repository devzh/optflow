#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time    : 2/6/18 9:54 PM
@Author  : Devzh
@Site    : www.devzh.cn
@File    : adamOptimizer.py
@Software: PyCharm
"""
import random
import numpy as np
from optflow.opt.optimizers import Optimizers


class Adam(Optimizers):
    def __init__(self, lr=0.0001, alpha=0.9, beta=0.99, batch_size=0, max_iter=100, epsilon=1e-8,
                 nesterov=False, **kwargs):
        super(Adam, self).__init__(batch_size=batch_size, max_iter=max_iter, name="Adam", **kwargs)
        self.nesterov = nesterov
        self.alpha = alpha
        self.beta = beta
        self.lr = lr
        self.epsilon = epsilon
        self.momentum_grad = None
        self.momentum_square = None

    def run(self):
        if self.graph is None:
            raise ValueError("optimizer's model is None, please init first!")

        last_rand = None
        print("rounds:\t", self.total_rounds)
        for i in range(0, self.total_rounds):
            rand = random.randint(0, self.epoch_size - 1)
            if rand != last_rand:
                data = self.graph.train_data[
                       int(rand * self.batch_size):
                       int((rand + 1) * self.batch_size), :]
                label = self.graph.train_label[
                        int(rand * self.batch_size):
                        int((rand + 1) * self.batch_size)]

                self.graph.set_running_data(data, label)
            self._run_once()
            # if i == 2:
            #     break
            last_rand = rand
            self._log()

    def get_updates(self):

        grads = self.graph.get_gradient()
        square = self.graph.get_grad_square()
        if self.momentum_grad is None:
            self.momentum_grad = 0.0 * grads
            self.momentum_square = 0.0 * square

        m = self.alpha * self.momentum_grad + (1 - self.alpha) * grads  # velocity
        v = self.beta * self.momentum_square + (1 - self.beta) * square
        self.momentum_grad = m
        self.momentum_square = v

        lr = -self.lr * np.sqrt(1 - np.power(self.beta, self.iterations)) / (
                1 - np.power(self.alpha, self.iterations))

        delta_grad = 0.0 * m
        for i in range(0, len(m)):
            delta_grad[i] = lr * m[i] / (np.sqrt(v[i]) + self.epsilon)

        return delta_grad
